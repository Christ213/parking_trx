package app

import (
	"database/sql"
	"parking_trx/repositories"
	"parking_trx/repositories/ouRepository"
	"parking_trx/repositories/productRepository"
	"parking_trx/repositories/trxRepository"
	"parking_trx/services"
)

func SetupApp(DBParking *sql.DB, DBMaster *sql.DB, repo repositories.Repository) services.UsecaseService {
	trxRepo := trxRepository.NewTrxRepository(repo)
	productRepo := productRepository.NewProductRepository(repo)
	ouRepo := ouRepository.NewOuRepository(repo)

	usecaseSvc := services.NewUsecaseService(
		DBParking, DBMaster, trxRepo, productRepo, ouRepo,
	)

	return usecaseSvc
}
