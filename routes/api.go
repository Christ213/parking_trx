package routes

import (
	"net/http"
	"parking_trx/config"
	"parking_trx/constants"
	"parking_trx/services"
	"parking_trx/services/trxService"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func RoutesApi(e echo.Echo, usecaseSvc services.UsecaseService) {
	public := e.Group("")
	private := e.Group("")
	private.Use(middleware.JWT([]byte(config.GetEnv("JWT_KEY"))))
	private.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowCredentials: constants.TRUE_VALUE,
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	trxGroup := public.Group("/trx")
	trxSvc := trxService.NewTrxService(usecaseSvc)
	trxGroup.POST("/update", trxSvc.UpdateTrx)
}
