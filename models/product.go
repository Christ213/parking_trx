package models

type Product struct {
	ID          int64  `json:"id"`
	ProductCode string `json:"productCode"`
	ProductName string `json:"productName"`
}
