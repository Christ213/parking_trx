package models

type Trx struct {
	DocNO       string  `json:"docNo"`
	DocDate     string  `json:"docDate"`
	OuID        int64   `json:"ouID"`
	OuCode      string  `json:"ouCode"`
	OuName      string  `json:"ouName"`
	ProductID   int64   `json:"productID"`
	ProductCode string  `json:"productCode"`
	ProductName string  `json:"productName"`
	GrandTotal  float64 `json:"grandTotal"`
	ServiceFee  float64 `json:"serviceFee"`
	MDR         float64 `json:"mdr"`
	Status      string  `json:"status"`
	StatusDesc  string  `json:"statusDesc"`
}

type RequestUpdateTrx struct {
	DocNO      string  `json:"docNo" validate:"required"`
	DocDate    string  `json:"docDate" validate:"required"`
	OuID       int64   `json:"ouID" validate:"required"`
	ProductID  int64   `json:"productID" validate:"required"`
	GrandTotal float64 `json:"grandTotal" validate:"required"`
	ServiceFee float64 `json:"serviceFee" validate:"required"`
	MDR        float64 `json:"mdr" validate:"required"`
	Status     string  `json:"status" validate:"required"`
	StatusDesc string  `json:"statusDesc" validate:"required"`
}
