package models

type OU struct {
	ID     int64  `json:"id"`
	OuCode string `json:"ouCode"`
	OuName string `json:"ouName"`
}
