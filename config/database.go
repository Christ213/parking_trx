package config

import (
	"database/sql"
	"errors"
	"fmt"

	_ "github.com/lib/pq"
)

var db_parking *sql.DB
var db_master *sql.DB

// connect to db -> parking
func OpenConnectionParking() error {
	var err error
	db_parking, err = setupConnectionParking()

	return err
}

func setupConnectionParking() (*sql.DB, error) {
	var connection = fmt.Sprintf(`user=%s password=%s dbname=%s host=%s port=%s sslmode=%s`, DBUser, DBPass, DBNameParking, DBHost, DBPort, SSLMode)
	fmt.Println("Connection Info :", DBDriver, connection)

	db_parking, err := sql.Open(DBDriver, connection)
	if err != nil {
		return db_parking, errors.New(fmt.Sprintf("Connection closed : Failed Connect Database", DBNameParking))
	}

	return db_parking, nil
}

func ClosedConnectionDBParking() {
	db_parking.Close()
}

func DBConnectionParking() *sql.DB {
	return db_parking
}

// connect to db -> master
func OpenConnectionMaster() error {
	var err error
	db_master, err = setupConnectionMaster()

	return err
}

func setupConnectionMaster() (*sql.DB, error) {
	var connection = fmt.Sprintf(`user=%s password=%s dbname=%s host=%s port=%s sslmode=%s`, DBUser, DBPass, DBNameMaster, DBHost, DBPort, SSLMode)
	fmt.Println("Connection Info :", DBDriver, connection)

	db_master, err := sql.Open(DBDriver, connection)
	if err != nil {
		return db_master, errors.New(fmt.Sprintf("Connection closed : Failed Connect Database", DBNameMaster))
	}

	return db_master, nil
}

func ClosedConnectionDBMaster() {
	db_master.Close()
}

func DBConnectionMaster() *sql.DB {
	return db_master
}
