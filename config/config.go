package config

import (
	"os"
	"parking_trx/constants"

	"github.com/joho/godotenv"
)

var (
	DBDriver      = GetEnv("DB_DRIVER")
	DBNameParking = GetEnv("DB_NAME_PARKING")
	DBNameMaster  = GetEnv("DB_NAME_MASTER")
	DBHost        = GetEnv("DB_HOST")
	DBPort        = GetEnv("DB_PORT")
	DBUser        = GetEnv("DB_USER")
	DBPass        = GetEnv("DB_PASS")
	SSLMode       = GetEnv("SSL_MODE")

	APPUrl    = GetEnv("APP_URL")
	APPPort   = GetEnv("APP_PORT")
	APPPrefix = GetEnv("APP_PREFIX")
)

func GetEnv(key string, value ...string) string {
	if err := godotenv.Load(); err != nil {
		panic("Error Load file .env not found")
	}

	if os.Getenv(key) != constants.EMPTY_VALUE {
		return os.Getenv(key)
	} else {
		if len(value) > constants.EMPTY_VALUE_INT {
			return value[constants.EMPTY_VALUE_INT]
		}
		return ""
	}
}
