package trxRepository

import (
	"database/sql"
	"errors"
	"parking_trx/constants"
	"parking_trx/models"
	"parking_trx/repositories"
	. "parking_trx/utils"
)

type trxRepository struct {
	RepoDB repositories.Repository
}

func NewTrxRepository(repoDB repositories.Repository) trxRepository {
	return trxRepository{
		RepoDB: repoDB,
	}
}

var defineColumn = ` doc_no, doc_date, ou_id, ou_code, ou_name, product_id, product_code, product_name, grand_total, service_fee, mdr, status, status_desc `

func (ctx trxRepository) FindTrxByDocNO(docNO string) (models.Trx, error) {
	var result models.Trx

	query := `SELECT ` + defineColumn + ` FROM trx WHERE doc_no = $1`
	rows, err := ctx.RepoDB.DBParking.Query(query, docNO)
	if err != nil {
		return result, err
	}

	defer rows.Close()
	data, err := trxDto(rows)
	if err != nil {
		return result, err
	}

	if len(data) == constants.EMPTY_VALUE_INT {
		return result, errors.New("trx not found")
	}

	return data[0], nil
}

func (ctx trxRepository) UpdateTrx(trx models.Trx) error {
	query := `UPDATE trx SET 
						doc_date = $1,
						ou_id = $2,
						ou_code = $3,
						ou_name = $4,
						product_id = $5,
						product_code = $6,
						product_name = $7,
						grand_total = $8,
						service_fee = $9,
						mdr = $10,
						status = $11,
						status_desc = $12,
						updated_at = $13,
						updated_by = ''
						WHERE doc_no = $14`

	_, err := ctx.RepoDB.DBParking.Exec(query, trx.DocDate, trx.OuID, trx.OuCode, trx.OuName, trx.ProductID,
		trx.ProductCode, trx.ProductName, trx.GrandTotal, trx.ServiceFee, trx.MDR, trx.Status, trx.StatusDesc, TimeStampNow(), trx.DocNO)

	if err != nil {
		return err
	}

	return nil
}

func trxDto(rows *sql.Rows) ([]models.Trx, error) {
	var result []models.Trx
	for rows.Next() {
		var trx models.Trx

		err := rows.Scan(&trx.DocNO, &trx.DocDate, &trx.OuID, &trx.OuCode, &trx.OuName, &trx.ProductID, &trx.ProductCode, &trx.ProductName,
			&trx.GrandTotal, &trx.ServiceFee, &trx.MDR, &trx.Status, &trx.StatusDesc)
		if err != nil {
			return result, err
		}

		result = append(result, trx)
	}
	return result, nil
}
