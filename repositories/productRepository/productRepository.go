package productRepository

import (
	"database/sql"
	"errors"
	"parking_trx/constants"
	"parking_trx/models"
	"parking_trx/repositories"
)

type productRepository struct {
	RepoDB repositories.Repository
}

func NewProductRepository(repoDB repositories.Repository) productRepository {
	return productRepository{
		RepoDB: repoDB,
	}
}

var defineColumn = ` product_code, product_name `

func (ctx productRepository) FindProductByID(id int64) (models.Product, error) {
	var result models.Product

	query := `SELECT id, ` + defineColumn + ` FROM product WHERE id = $1`
	rows, err := ctx.RepoDB.DBMaster.Query(query, id)
	if err != nil {
		return result, err
	}

	defer rows.Close()
	data, err := productDto(rows)
	if err != nil {
		return result, err
	}

	if len(data) == constants.EMPTY_VALUE_INT {
		return result, errors.New("product not found")
	}

	return data[0], nil
}

func productDto(rows *sql.Rows) ([]models.Product, error) {
	var result []models.Product
	for rows.Next() {
		var val models.Product

		err := rows.Scan(&val.ID, &val.ProductCode, &val.ProductName)
		if err != nil {
			return result, err
		}

		result = append(result, val)
	}
	return result, nil
}
