package repositories

import (
	"context"
	"database/sql"
)

type Repository struct {
	DBParking *sql.DB
	DBMaster  *sql.DB
	Context   context.Context
}

func NewRepository(conn1 *sql.DB, conn2 *sql.DB, ctx context.Context) Repository {
	return Repository{
		DBParking: conn1,
		DBMaster:  conn2,
		Context:   ctx,
	}
}
