package repositories

import "parking_trx/models"

type TrxRepository interface {
	UpdateTrx(trx models.Trx) error
	FindTrxByDocNO(docNO string) (models.Trx, error)
}

type OuRepository interface {
	FindOuByID(id int64) (models.OU, error)
}

type ProductRepository interface {
	FindProductByID(id int64) (models.Product, error)
}
