package ouRepository

import (
	"database/sql"
	"errors"
	"parking_trx/constants"
	"parking_trx/models"
	"parking_trx/repositories"
)

type ouRepository struct {
	RepoDB repositories.Repository
}

func NewOuRepository(repoDB repositories.Repository) ouRepository {
	return ouRepository{
		RepoDB: repoDB,
	}
}

var defineColumn = ` ou_code, ou_name `

func (ctx ouRepository) FindOuByID(id int64) (models.OU, error) {
	var result models.OU

	query := `SELECT id, ` + defineColumn + ` FROM ou WHERE id = $1`
	rows, err := ctx.RepoDB.DBMaster.Query(query, id)
	if err != nil {
		return result, err
	}

	defer rows.Close()
	data, err := ouDto(rows)
	if err != nil {
		return result, err
	}

	if len(data) == constants.EMPTY_VALUE_INT {
		return result, errors.New("ou not found")
	}

	return data[0], nil
}

func ouDto(rows *sql.Rows) ([]models.OU, error) {
	var result []models.OU
	for rows.Next() {
		var val models.OU

		err := rows.Scan(&val.ID, &val.OuCode, &val.OuName)
		if err != nil {
			return result, err
		}

		result = append(result, val)
	}
	return result, nil
}
