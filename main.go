package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"parking_trx/app"
	"parking_trx/config"
	"parking_trx/constants"
	"parking_trx/helpers"
	"parking_trx/repositories"
	"parking_trx/routes"
	"strconv"

	"github.com/go-playground/locales/id"
	ut "github.com/go-playground/universal-translator"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gopkg.in/go-playground/validator.v9"
	id_translations "gopkg.in/go-playground/validator.v9/translations/id"
)

type CustomValidator struct {
	validator  *validator.Validate
	translator ut.Translator
}

var (
	uni         *ut.UniversalTranslator
	echoHandler echo.Echo
)

var ctx = context.Background()

func (cv *CustomValidator) Validate(i interface{}) error {
	err := cv.validator.Struct(i)
	if err != nil {
		errs := err.(validator.ValidationErrors)
		for _, row := range errs {
			return errors.New(row.Translate(cv.translator))
		}
	}

	return cv.validator.Struct(i)
}

func init() {
	boardingService()

	e := echo.New()
	echoHandler = *e
	validateCustom := validator.New()

	id := id.New()
	uni = ut.New(id, id)
	trans, _ := uni.GetTranslator("id")
	id_translations.RegisterDefaultTranslations(validateCustom, trans)
	e.Validator = &CustomValidator{validator: validateCustom, translator: trans}

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.Secure())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowCredentials: constants.TRUE_VALUE,
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	e.HTTPErrorHandler = func(err error, ctx echo.Context) {
		report, ok := err.(*echo.HTTPError)
		if !ok {
			report = echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		result := helpers.ResponseJSON(constants.FALSE_VALUE, strconv.Itoa(report.Code), err.Error(), nil)
		ctx.Logger().Error(report)
		ctx.JSON(report.Code, result)
	}
}

func main() {
	if err := config.OpenConnectionParking(); err != nil {
		panic(fmt.Sprintf("Open Connection Failed : %s", err.Error()))
	}
	defer config.ClosedConnectionDBParking()

	DBParking := config.DBConnectionParking()

	if err := config.OpenConnectionMaster(); err != nil {
		panic(fmt.Sprintf("Open Connection Failed : %s", err.Error()))
	}
	defer config.ClosedConnectionDBMaster()

	DBMaster := config.DBConnectionMaster()

	repo := repositories.NewRepository(DBParking, DBMaster, ctx)

	services := app.SetupApp(DBParking, DBMaster, repo)

	routes.RoutesApi(echoHandler, services)
	echoHandler.Use(middleware.Logger())
	port := fmt.Sprintf(":%s", config.GetEnv("APP_PORT"))
	echoHandler.Logger.Fatal(echoHandler.Start(port))
}

func boardingService() {
	fmt.Println(`                                                                                                                                                                             
##   ##  ##  ##  #####     #####   ##  ##  ##      ##  ##  ##  ######   ########  ##   ######  ##  ##  ######  ########  ##  ##  ##   ######
### ###  ## ##   ##  ##   ##   ##  ### ##  ##      ##  ### ##  ##          ##     ##  ##       ## ##   ##         ##     ##  ### ##  ##    
## # ##  ####    #####    ##   ##  ######  ##      ##  ######  ######      ##     ##  ##       ####    ######     ##     ##  ######  ##   ###
##   ##  ## ##   ##       ##   ##  ## ###  ##      ##  ## ###  ##          ##     ##  ##       ## ##   ##         ##     ##  ## ###  ##    ##
##   ##  ##  ##  ##        #####   ##  ##  ######  ##  ##  ##  ######      ##     ##   ######  ##  ##  ######     ##     ##  ##  ##   ######`)
}
