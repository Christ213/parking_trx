package trxService

import (
	"fmt"
	"log"
	"net/http"
	"parking_trx/constants"
	"parking_trx/helpers"
	"parking_trx/models"
	"parking_trx/services"

	"github.com/labstack/echo"
)

type trxService struct {
	Service services.UsecaseService
}

func NewTrxService(service services.UsecaseService) trxService {
	return trxService{
		Service: service,
	}
}

func (svc trxService) UpdateTrx(ctx echo.Context) error {
	var result models.Response

	request := new(models.RequestUpdateTrx)
	if err := helpers.BindValidateStruct(ctx, request); err != nil {
		log.Println("Error Validate Request UpdateTrx", err.Error())
		result = helpers.ResponseJSON(constants.FALSE_VALUE, constants.VALIDATE_ERROR_CODE, err.Error(), nil)
		return ctx.JSON(http.StatusBadRequest, result)
	}

	_, err := svc.Service.TrxRepo.FindTrxByDocNO(request.DocNO)
	if err != nil {
		log.Println("Error UpdateTrx - FindTrxByDocNO : ", err.Error())
		result = helpers.ResponseJSON(constants.FALSE_VALUE, constants.VALIDATE_ERROR_CODE, fmt.Sprintf("Failed Update Data - Cannot Find Trx doc_no of %v", request.DocNO), nil)
		return ctx.JSON(http.StatusBadRequest, result)
	}

	ou, err := svc.Service.OuRepo.FindOuByID(request.OuID)
	if err != nil {
		log.Println("Error UpdateTrx - FindOuByID : ", err.Error())
		result = helpers.ResponseJSON(constants.FALSE_VALUE, constants.VALIDATE_ERROR_CODE, fmt.Sprintf("Failed Update Data - Cannot Find ou - ou_id of %v", request.OuID), nil)
		return ctx.JSON(http.StatusBadRequest, result)
	}

	product, err := svc.Service.ProductRepo.FindProductByID(request.ProductID)
	if err != nil {
		log.Println("Error UpdateTrx - FindProductByID : ", err.Error())
		result = helpers.ResponseJSON(constants.FALSE_VALUE, constants.VALIDATE_ERROR_CODE, fmt.Sprintf("Failed Update Data - Cannot Find product - product_id of %v", request.ProductID), nil)
		return ctx.JSON(http.StatusBadRequest, result)
	}

	trx := models.Trx{
		DocNO:       request.DocNO,
		DocDate:     request.DocDate,
		OuID:        request.OuID,
		OuCode:      ou.OuCode,
		OuName:      ou.OuName,
		ProductID:   request.ProductID,
		ProductCode: product.ProductCode,
		ProductName: product.ProductName,
		GrandTotal:  request.GrandTotal,
		ServiceFee:  request.ServiceFee,
		MDR:         request.MDR,
		Status:      request.Status,
		StatusDesc:  request.StatusDesc,
	}

	err = svc.Service.TrxRepo.UpdateTrx(trx)
	if err != nil {
		log.Println("Error UpdateTrx - UpdateTrx : ", err.Error())
		result = helpers.ResponseJSON(constants.FALSE_VALUE, constants.SYSTEM_ERROR_CODE, "Failed Update Data", nil)
		return ctx.JSON(http.StatusBadRequest, result)
	}

	log.Println("Reponse UpdateTrx ", "Success Update Data")
	result = helpers.ResponseJSON(constants.TRUE_VALUE, constants.SUCCESS_CODE, constants.EMPTY_VALUE, "Success Update Data")
	return ctx.JSON(http.StatusOK, result)
}
