package services

import (
	"database/sql"
	"parking_trx/repositories"
)

type UsecaseService struct {
	RepoDBParking *sql.DB
	RepoDBMaster  *sql.DB
	TrxRepo       repositories.TrxRepository
	ProductRepo   repositories.ProductRepository
	OuRepo        repositories.OuRepository
}

func NewUsecaseService(
	repoDBParking *sql.DB,
	repoDBMaster *sql.DB,
	trxRepo repositories.TrxRepository,
	productRepo repositories.ProductRepository,
	ouRepo repositories.OuRepository,
) UsecaseService {
	return UsecaseService{
		RepoDBParking: repoDBParking,
		RepoDBMaster:  repoDBMaster,
		TrxRepo:       trxRepo,
		ProductRepo:   productRepo,
		OuRepo:        ouRepo,
	}
}
